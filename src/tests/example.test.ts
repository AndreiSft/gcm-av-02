import req from "supertest";
import app from "../app";

test("[GET] /:name", async () => {
  const res = await req(app).get("/andrei");
  expect(res.text).toBe("Welcome andrei");
});
