import { Response, Request } from "express";

export const helloWorld = (_: Request, res: Response) => {
  res.send("Hello World");
};

export const welcome = (req: Request, res: Response) => {
  try {
    const { name } = req.params;

    if (name.length <= 3) {
      throw new Error("Very short name");
    }

    res.send(`Welcome ${name}`);
  } catch (error) {
    throw error.message;
  }
};
