import { Router } from "express";
import { helloWorld, welcome } from "./controllers/main.controller";

const routes = Router();

routes.get("/", helloWorld);
routes.get("/:name", welcome);

export default routes;
